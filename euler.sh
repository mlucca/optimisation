#!/bin/bash
#SBATCH --job-name=gymnasium        # Job name    (default: sbatch)
#SBATCH --output=gym.out            # Output file (default: slurm-%j.out)
#SBATCH --error=gym.err             # Error file  (default: slurm-%j.out)
#SBATCH --nodes=1                   # Number of nodes
#SBATCH --ntasks=5                  # Number of tasks
#SBATCH --ntasks-per-node=5         # Number of tasks per node
#SBATCH --constraint=EPYC_7763      # Select node with CPU
#SBATCH --mem-per-cpu=1024          # Memory per CPU
#SBATCH --time=4:00:00              # Wall clock time limit
#SBATCH --mail-type=END,FAIL        # Send an email when job ends

# Load some modules
module load stack/.2024-05-silent gcc/13.2.0
module load python/3.11.6
module load openmpi
module list

# Load environment
source retro/bin/activate
pip3 install torch gymnasium pygame box2d-py swig mpi4py

# Prepare Euler by emptying the data folder
cd model
cd benchmarking

# Run the program with the argument passed
export USE_MPI=1
if [ "$USE_MPI" == "1" ]; then
    # Run with mpirun
    mpirun python3 benchmark.py "$CMD_ARG"
else
    # Run normally with python3
    python3 benchmark.py $CMD_ARG
fi
