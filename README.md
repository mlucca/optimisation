# Optimisation

Feel free to delete the stuff in here, I created a gym branch for myself to start to try out


## Things to achieve and powerpoint

### Code

* Completion of at least 3 different generation function
* Compatibility between all the methods and normalisation


## Getting started

### Packages

Everythican be found in the retro environment which can be activated through


`source retro/bin/activate`


The packages necessary are

* python@3.10 or above


* gymnasium
* pytorch (torch)
* pygame
* box2d-py
* swig

### Once done installing

Use the command `python3 model/lunar_model.py -v` to see a visualisation of the AI, put nothing to just train

### Load an AI

In the file `model/neural_network/mlp.py ` when initialising the neural network, substitute the file name in the function self.load and see your trained AI working.


## How to use  Euler

### Setup

Paste this line of code after entering euler for a first setup

```bash
# allocate a job
salloc --ntasks=1 --constraint=EPYC_7763

# load some modules
module load stack/.2024-05-silent gcc/13.2.0
module load python/3.11.6
module load openmpi
module list

# load environment
source retro/bin/activate
pip3 install torch gymnasium pygame box2d-py swig mpi4py
```

### Copy results from Euler

Just use this simple code to let the magic happen

`scp -r ethz_user_name@euler.ethz.ch:optimisation/Results/ .`


### What to be aware of…

The amont of CPUs involved must be the same as the amount of runs involved.

## References

* [Gymnasium documentation](https://gymnasium.farama.org/search/?q=seed&check_keywords=yes&area=default)
* [Gym introduction](https://blog.paperspace.com/getting-started-with-openai-gym/)
* [Genetic algorithm](https://github.com/robertjankowski/ga-openai-gym/blob/master/scripts/cartpole/cartpole_mlp.py)
* [GA example](https://github.com/robertjankowski/ga-openai-gym/blob/master/scripts/cartpole/cartpole_mlp.py)


The genetic algorithm can be found in the folder `ga_gym_old.` This uses the old version of `gymnasium`, denoted as `gym`.


