#!/bin/bash

# Define the arguments
# args=("--lunar1" "--lunar2" "--lunar3")
# args=("--bipedal1" "--bipedal2" "--bipedal3")
# args=("--acrobot1" "--acrobot2" "--acrobot3")
args=("--lunar1" "--lunar2" "--lunar3" 
    #   "--acrobot1" "--acrobot2" "--acrobot3"
      "--cartpole1" "--cartpole2" "--cartpole3"
      "--bipedal1" "--bipedal2" "--bipedal3")

# Loop over the arguments and submit the SLURM job for each one
for arg in "${args[@]}"
do
    export CMD_ARG=$arg
    sbatch euler.sh
    sleep 60
done