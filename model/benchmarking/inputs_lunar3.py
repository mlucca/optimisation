from benchmarking.inputs import *

"""
    Test:
        Test the crossover rate
        to activate this input, add --lunar3 after benchmark.py
"""

# Define new values to overwrite the original settings
new_settings = [
    ('game', "lunar_lander"),
    ('plot_single', True),
    ('compare_all_plot', True),
    ('incremental_saving', True),
    ('save_as_pytorch', False),
    ('anti_stagnation', False),
    ('use_temp', False),
    ('visualize_best_at_end', False),
    ('only_better', False),
    ('scale_by_weight', False),
]

new_functions = [
    ('generation', ["generation_partitioned"]),   
    ('selection', ["ranking_selection"]),
    ('crossover', ["crossover_rand"]),
    ('mutation', ["mutation_rand_all"]),
]

new_parameters = [
    ('max_generation', [1500]),
    ('crossover_rate', [0.1,0.3,0.5,0.7,0.9]),
]

# Function to update the original list with new values
def update_settings(original, new):
    original_dict = dict(original)
    for key, value in new:
        original_dict[key] = value
    return list(original_dict.items())

# Update the original settings with the new values
INPUT_SETTINGS = update_settings(INPUT_SETTINGS, new_settings)
INPUT_FUNCTIONS = update_settings(INPUT_FUNCTIONS, new_functions)
INPUT_PARAMETERS = update_settings(INPUT_PARAMETERS, new_parameters)