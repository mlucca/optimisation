import copy
import sys
import os
import io
import contextlib
from itertools import product
from collections import namedtuple
from datetime import datetime
import torch
import cProfile
import shutil

import gymnasium as gym
import numpy as np

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(current_dir, '..')
sys.path.append(parent_dir)

from typing import Tuple
from genetic_algorithm.individual import *
from genetic_algorithm.population import Population
from neural_network.base_nn import NeuralNetwork
from neural_network.mlp import DeepMLPTorch
import games

import importlib

# Define the suffix based on a command-line argument or any other logic
default_suffix = ''
valid_suffixes = ['--lunar1', '--lunar2', '--lunar3', 
                  '--bipedal1', '--bipedal2', '--bipedal3',
                  '--cartpole1', '--cartpole2', '--cartpole3',
                  '--racing1', '--racing2', '--racing3',
                  '--acrobot1', '--acrobot2', '--acrobot3']

# Check for command-line arguments and determine the suffix
config_suffix = default_suffix
for arg in sys.argv:
    if arg in valid_suffixes:
        config_suffix = arg.replace('--', '_')
        break

# Construct the full module name
module_name = f'benchmarking.inputs{config_suffix}'

# Dynamically import the module
inputs_module = importlib.import_module(module_name)

# Optionally, set the imported attributes to the local namespace
for attr in dir(inputs_module):
    if not attr.startswith("__"):
        globals()[attr] = getattr(inputs_module, attr)

def print_full_line(character='-'):
    columns, _ = shutil.get_terminal_size()
    print(character * columns)

def benchmark():

    function_categories = [item[0] for item in INPUT_FUNCTIONS]
    parameter_categories = [item[0] for item in INPUT_PARAMETERS]
    settings_categories = [item[0] for item in INPUT_SETTINGS]
    settings_categories.extend(["output_folder", "output_file"])
    
    function_values = [item[1] for item in INPUT_FUNCTIONS]
    parameter_values = [item[1] for item in INPUT_PARAMETERS]
    settings_values = [item[1] for item in INPUT_SETTINGS]
    
    combinations_of_functions = list(product(*function_values))
    combinations_of_parameters = list(product(*parameter_values))
    
    # Define base named tuples
    BaseFunctions = namedtuple('Functions', function_categories)
    BaseParameters = namedtuple('Parameters', parameter_categories)
    BaseSettings = namedtuple('Settings', settings_categories)
    
    game = settings_values[0]
    
    # Define custom classes to override the __repr__ method
    class Functions(BaseFunctions):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Functions = [\n\n    {fields}\n\n ]'
    
    class Parameters(BaseParameters):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Parameters = [\n\n    {fields}\n\n ]'
        
    class Settings(BaseSettings):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Settings = [\n\n    {fields}\n\n ]'
    
    # Get current date and time
    current_date = datetime.now().strftime("%Y-%m-%d")
    current_time = datetime.now().strftime("%H-%M-%S")

    # Create the date folder inside Results
    date_folder = os.path.join("../../", "Results", current_date)
    os.makedirs(date_folder, exist_ok=True)

    # Create the game folder inside the date folder
    output_folder = os.path.join(date_folder, f"{game}_{current_time}")
    os.makedirs(output_folder, exist_ok=True)
    
    run_id = 1
    total_runs = len(combinations_of_functions) * len(combinations_of_parameters)
    
    print("\n" + "Benchmarking started..." + "\n")
    
    for func in combinations_of_functions:
        for para in combinations_of_parameters:
            # Creating the output File
            output_file = os.path.join(output_folder, f"run_{run_id}")
            settings_values.extend([output_folder, output_file])
            
            SETTINGS = Settings(*settings_values)
            FUNCTIONS = Functions(*func)
            PARAMETERS = Parameters(*para)
            
            settings_values.pop()
            settings_values.pop() # Why there is twice
            
            if SETTINGS.log:
                with open(output_file + ".out", 'a') as f:
                    f.write('=' * shutil.get_terminal_size().columns + "\n")
                    f.write("INPUTS:\n")
                    f.write('=' * shutil.get_terminal_size().columns + "\n")
                    f.write("\n " + repr(SETTINGS) + "\n\n " + repr(FUNCTIONS) + "\n\n " + repr(PARAMETERS) + "\n\n")
                    f.write('=' * shutil.get_terminal_size().columns + "\n")
                    f.write("OUTPUTS:" + "\n")
                    f.write('=' * shutil.get_terminal_size().columns + "\n\n")
            
            if SETTINGS.verbose:
                print("\n")
                print_full_line('=')
                print("INPUTS:")
                print_full_line('=')
                print("\n", SETTINGS, "\n\n", FUNCTIONS, "\n\n", PARAMETERS, "\n")
            print_full_line('=')
            print(f"Run {run_id} out of {total_runs} started...")
            print_full_line('=')
            
            if SETTINGS.verbose:
                print("OUTPUTS:")
                print_full_line('=')
            
            if game == "lunar_lander":
                games.lunar_lander_main(FUNCTIONS, PARAMETERS, SETTINGS)
            elif game == "car_racing":
                games.car_racing_main(FUNCTIONS, PARAMETERS, SETTINGS)
            elif game == "bipedal_walker":
                games.bipedal_walker_main(FUNCTIONS, PARAMETERS, SETTINGS)
            elif game == "acrobot":
                games.acrobot_main(FUNCTIONS, PARAMETERS, SETTINGS)
            elif game == "cart_pole":
                games.cart_pole_main(FUNCTIONS, PARAMETERS, SETTINGS)
            
            print_full_line('=')
            
            if SETTINGS.plot_single:
                print("\n" + "Plotting fitness function...")
                os.system(f"python ../../model/util/plot_fitness_function.py {output_file}")
                print("\n" + "Plotting complete..." + "\n")
                
            if SETTINGS.visualize_best_at_end:
                print("\n" + "Visualizing best solution...")
                os.system(f"python ../../model/util/visualize_solution.py {output_file} 10")
                print("\n" + "Visualization complete..." + "\n")

            run_id += 1
    
    if SETTINGS.compare_all_plot:
        print("\n" + "Plotting all fitness functions...")
        os.system(f"python ../../model/util/plot_multiple_fitness_functions.py {output_folder}")
        print("\n" + "Plotting complete..." + "\n")

def benchmark_MPI(comm, size, rank):
    function_categories = [item[0] for item in INPUT_FUNCTIONS]
    parameter_categories = [item[0] for item in INPUT_PARAMETERS]
    settings_categories = [item[0] for item in INPUT_SETTINGS]
    settings_categories.extend(["output_folder", "output_file"])
    
    function_values = [item[1] for item in INPUT_FUNCTIONS]
    parameter_values = [item[1] for item in INPUT_PARAMETERS]
    settings_values = [item[1] for item in INPUT_SETTINGS]
    
    combinations_of_functions = list(product(*function_values))
    combinations_of_parameters = list(product(*parameter_values))
    
    # Define base named tuples
    BaseFunctions = namedtuple('Functions', function_categories)
    BaseParameters = namedtuple('Parameters', parameter_categories)
    BaseSettings = namedtuple('Settings', settings_categories)
    
    game = settings_values[0]
    
    # Define custom classes to override the __repr__ method
    class Functions(BaseFunctions):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Functions = [\n\n    {fields}\n\n ]'
    
    class Parameters(BaseParameters):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Parameters = [\n\n    {fields}\n\n ]'
        
    class Settings(BaseSettings):
        def __repr__(self):
            max_len = max(len(name) for name in self._fields)
            fields = ',\n    '.join(f'{name.ljust(max_len)} = {repr(value)}' for name, value in zip(self._fields, self))
            return f'Settings = [\n\n    {fields}\n\n ]'
    
    # Get current date and time
    current_date = datetime.now().strftime("%Y-%m-%d")
    current_time = datetime.now().strftime("%H-%M-%S")

    # Create the date folder inside Results
    date_folder = os.path.join("../../", "Results", current_date)
    os.makedirs(date_folder, exist_ok=True)

    # Create the game folder inside the date folder
    output_folder = os.path.join(date_folder, f"{game}_{current_time}")
    os.makedirs(output_folder, exist_ok=True)
    
    # Distribute tasks among processes
    run_id = 1
    total_runs = len(combinations_of_functions) * len(combinations_of_parameters)
    
    if rank == 0:
        print("\n" + "Benchmarking started..." + "\n")
    
    tasks = [(func, para, run_id + i) for i, (func, para) in enumerate(product(combinations_of_functions, combinations_of_parameters))]
    tasks_per_rank = len(tasks) // size
    if rank == 0:
        tasks = [tasks[i * tasks_per_rank:(i + 1) * tasks_per_rank] for i in range(size)]
    else:
        tasks = None
    
    tasks = comm.scatter(tasks, root=0)
    
    for func, para, run_id in tasks:
        # Creating the output File
        output_file = os.path.join(output_folder, f"run_{run_id}")
        settings_values.extend([output_folder, output_file])
        
        SETTINGS = Settings(*settings_values)
        FUNCTIONS = Functions(*func)
        PARAMETERS = Parameters(*para)
        
        settings_values.pop()
        settings_values.pop()
        
        if SETTINGS.log:
            with open(output_file + ".out", 'a') as f:
                f.write('=' * shutil.get_terminal_size().columns + "\n")
                f.write("INPUTS:\n")
                f.write('=' * shutil.get_terminal_size().columns + "\n")
                f.write("\n " + repr(SETTINGS) + "\n\n " + repr(FUNCTIONS) + "\n\n " + repr(PARAMETERS) + "\n\n")
                f.write('=' * shutil.get_terminal_size().columns + "\n")
                f.write("OUTPUTS:" + "\n")
                f.write('=' * shutil.get_terminal_size().columns + "\n\n")
        
        if SETTINGS.verbose:
            print("\n")
            print_full_line('=')
            print("INPUTS:")
            print_full_line('=')
            print("\n", SETTINGS, "\n\n", FUNCTIONS, "\n\n", PARAMETERS, "\n")
        print_full_line('=')
        print(f"Run {run_id} out of {total_runs} started...")
        print_full_line('=')
        
        if SETTINGS.verbose:
            print("OUTPUTS:")
            print_full_line('=')
        
        if game == "lunar_lander":
            games.lunar_lander_main(FUNCTIONS, PARAMETERS, SETTINGS)
        elif game == "car_racing":
            games.car_racing_main(FUNCTIONS, PARAMETERS, SETTINGS)
        elif game == "bipedal_walker":
            games.bipedal_walker_main(FUNCTIONS, PARAMETERS, SETTINGS)
        elif game == "acrobot":
            games.acrobot_main(FUNCTIONS, PARAMETERS, SETTINGS)
        elif game == "cart_pole":
                games.cart_pole_main(FUNCTIONS, PARAMETERS, SETTINGS)
        
        print_full_line('=')
        
        if SETTINGS.plot_single:
            print("\n" + "Plotting fitness function...")
            os.system(f"python ../../model/util/plot_fitness_function.py {output_file}")
            print("\n" + "Plotting complete..." + "\n")
            
        if SETTINGS.visualize_best_at_end:
            print("\n" + "Visualizing best solution...")
            os.system(f"python ../../model/util/visualize_solution.py {output_file} 10")
            print("\n" + "Visualization complete..." + "\n")
    
    if rank == 0:
        for _ in range(size - 1):
            comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
        print("\n" + "Benchmarking completed..." + "\n")
        if SETTINGS.compare_all_plot:
            print("\n" + "Plotting all fitness functions...")
            os.system(f"python ../../model/util/plot_multiple_fitness_functions.py {output_folder}")
            print("\n" + "Plotting complete..." + "\n")
    else:
        comm.send(None, dest=0)

    
if __name__ == "__main__":

    """
        MPI4PY

    Description:
            Script to activate multi CPU processing 

    Usage:
        export USE_MPI=1
        1 to activate
        0 to deactivate
    """      
    use_mpi = os.getenv('USE_MPI')
    if use_mpi:
        try:
            from mpi4py import MPI
            print("MPI is imported.")

            # Get COMMON WORLD communicator, size & rank
            comm = MPI.COMM_WORLD
            size = comm.Get_size()
            rank = comm.Get_rank()

            benchmark_MPI(comm, size, rank)

            # Finalize scheduling task
            MPI.Finalize()
        except ImportError:
            print("MPI import failed. Probably mpi4py is not installed")
    else:
        print("Running without MPI.")
        benchmark()
