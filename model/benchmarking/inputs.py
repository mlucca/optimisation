INPUT_SETTINGS = [('game', "cart_pole"),
                  ('verbose', True), # Print out the values for the generation
                  ('log', True),
                  ('log_frequency', 1),
                  ('plot_single', False),
                  ('compare_all_plot', True),
                  ('incremental_saving', False),
                  ('save_as_pytorch', False),
                  ('anti_stagnation', False),
                  ('use_temp', True),
                  ('load_weights_path', ""),
                  ('visualize_best_at_end', True),
                  ('only_better', False),
                  ('scale_by_weight', False),
                  ('diversity', False)
                  ]

INPUT_FUNCTIONS = [('run', ["run", "run2"]), 
                   ('generation', ["generation_simp"]),
                   # [generation, generation_simp, generation_random, generation_partitioned, generation_best_two]
                   ('selection', ["selection_power_all"]),
                   # [ranking_selection, ranking_selectionMat, selection_power_all]
                   ('crossover', ["crossover_randMat"]),
                   # [crossover_rand, crossover_randMat]
                   ('mutation', ["mutation_rand_all_para"]),
                   # [mutation_rand_single, mutation_rand_all, mutation_rand_all_para] 
                  ]

INPUT_PARAMETERS = [('population_size', [32]),
                    ('max_generation', [50]),
                    ('mutation_rate', [0.02]),
                    ('crossover_rate', [0.5]),
                    ('start_temp', [100]),
                    ('decay_temp', [0.02]),
                    ('keep_best', [2]),
                    ('calc_fitness_rep', [1]),
                    ('selection_pow', [1.2]),
                   ]

