from benchmarking.inputs import *

"""
    Test:
        Test the types of generations
        to activate this input, add --cartpole1 after benchmark.py
"""

# Define new values to overwrite the original settings
new_settings = [
    ('game', "cart_pole"),
    ('plot_single', True),
    ('compare_all_plot', True),
    ('incremental_saving', True),
    ('save_as_pytorch', False),
    ('visualize_best_at_end', False),
    ('use_temp', False),
    ('scale_by_weight', False),
]

new_functions = [
    ('generation', ["generation_random", "generation_partitioned", "generation_best_two"]),   
    ('selection', ["ranking_selection"]),
    ('crossover', ["crossover_rand"]),
    ('mutation', ["mutation_rand_all"]),
]

new_parameters = [
    ('population_size', [32]),
    ('max_generation', [1500]),
    ('start_temp', [50]),
]

# Function to update the original list with new values
def update_settings(original, new):
    original_dict = dict(original)
    for key, value in new:
        original_dict[key] = value
    return list(original_dict.items())

# Update the original settings with the new values
INPUT_SETTINGS = update_settings(INPUT_SETTINGS, new_settings)
INPUT_FUNCTIONS = update_settings(INPUT_FUNCTIONS, new_functions)
INPUT_PARAMETERS = update_settings(INPUT_PARAMETERS, new_parameters)