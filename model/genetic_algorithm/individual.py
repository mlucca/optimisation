from abc import ABC, abstractmethod
from typing import List, Tuple


import numpy as np

from neural_network.base_nn import NeuralNetwork


class Individual(ABC):
    def __init__(self):
        self.neural_network = self.get_model()
        self.fitness = 0.0
        self.weights_biases: np.array = self.neural_network.get_weights_biases()

    def calculate_fitness(self, env, rep:int=1) -> None:
        total_fitness = 0

        for _ in range(rep):
            fitness, weights_biases = self.run_single(env)
            total_fitness += fitness

        self.fitness = total_fitness / rep
        self.weights_biases = weights_biases

    def update_model(self) -> None:
        self.neural_network.update_weights_biases(self.weights_biases)

    @abstractmethod
    def get_model(self) -> NeuralNetwork:
        pass

    @abstractmethod
    def run_single(self, env, n_episodes=300, render=False) -> Tuple[float, np.array]:
        pass

def crossover(parent1_weights_biases: np.array, parent2_weights_biases: np.array, p: float):
    position = np.random.randint(0, parent1_weights_biases.shape[0])
    child1_weights_biases = np.copy(parent1_weights_biases)
    child2_weights_biases = np.copy(parent2_weights_biases)

    if np.random.rand() < p:
        child1_weights_biases[position:], child2_weights_biases[position:] = \
            child2_weights_biases[position:], child1_weights_biases[position:]
    return child1_weights_biases, child2_weights_biases

def crossover_rand(parent1_weights_biases: np.array, parent2_weights_biases: np.array, p: float):
    rand_filter = np.squeeze([np.random.rand(len(parent1_weights_biases)) > 0.5])
    child1_weights_biases = np.copy(parent1_weights_biases)
    child2_weights_biases = np.copy(parent2_weights_biases)

    if np.random.rand() < p:
        child1_weights_biases[rand_filter], child2_weights_biases[rand_filter] = \
            child2_weights_biases[rand_filter], child1_weights_biases[rand_filter]
    return child1_weights_biases, child2_weights_biases

# This function assumes that the population is even
def crossover_randMat(parents_wb: np.array, p_crossover: float, start: int):
    # get dimensions
    dim0 = int((parents_wb.shape[0] - start) / 2) # assume each child has 2 parents
    dim1 = parents_wb.shape[1]

    # each weight/bias has a 50% chance of being swapped
    rand_cross_loc = np.random.rand(dim0, dim1) < np.random.rand()
    # total chance of crossover happening is regulated by p_crossover
    rand_p = np.random.rand(dim0, 1) < p_crossover
    # if rand_p False then set rand_cross_loc all to False
    # -> only do crossover with a probability of p_crossover
    rand_filter = np.multiply(rand_cross_loc, rand_p)

    children_wb = np.copy(parents_wb)

    # temporary copy as will be overwritten in next step
    tmp = np.copy(children_wb[start:start+dim0,:][rand_filter])
    # set first dim0 are replaced with the last dim0 if they fullfil rand_filter
    children_wb[start:start+dim0][rand_filter] = children_wb[start+dim0:][rand_filter]
    # the last dim0 are replaced with the first dim0 if they fulllfil rand_filter
    children_wb[start+dim0:][rand_filter] = tmp

    return children_wb



# def crossover_new(parent1_weights_biases: np.array, parent2_weights_biases: np.array):
#     """
#     Crossover is calculated only if random.randn() < p
#     """
#     position = np.random.randint(0, parent1_weights_biases.shape[0])
#     child1_weights_biases = np.copy(parent1_weights_biases)
#     child2_weights_biases = np.copy(parent2_weights_biases)

#     child1_weights_biases[position:], child2_weights_biases[position:] = \
#         child2_weights_biases[position:], child1_weights_biases[position:]
#     return child1_weights_biases, child2_weights_biases

def inversion(child_weights_biases: np.array):
    return child_weights_biases[::-1]

# def mutation_gen(child_weights_biases: np.array, p_mutation):
#     """
#     Given `p_mutation` change each value in child_weights_biases
#     """
#     for i in range(len(child_weights_biases)):
#         if np.random.rand() < p_mutation:
#             child_weights_biases[i] = np.random.uniform(-100, 100)


# def mutation(parent_weights_biases: np.array, p: float, scale=10):
#     child_weight_biases = np.copy(parent_weights_biases)
#     if np.random.rand() < p:
#         position = np.random.randint(0, parent_weights_biases.shape[0])
#         n = np.random.normal(np.mean(child_weight_biases), np.std(child_weight_biases))
#         child_weight_biases[position] = n + np.random.randint(-scale, scale)
#     return child_weight_biases

def mutation_rand_single(parent_weights_biases: np.array, p: float, scale=10):
    child_weight_biases = np.copy(parent_weights_biases)
    if np.random.rand() < p:
        position = np.random.randint(0, parent_weights_biases.shape[0])
        #child_weight_biases[position] += child_weight_biases[position] * np.random.normal(scale=scale)
        child_weight_biases[position] = np.random.normal(child_weight_biases[position], scale=scale)
    return child_weight_biases

def mutation_rand_all(parent_weights_biases: np.array, p: float, scale=6):
    child_weight_biases = np.copy(parent_weights_biases)

    dim = len(child_weight_biases)
    # randMat = np.random.normal(scale=scale, size=dim)
    randMat = np.random.rand(dim) * 2 * scale - scale
    rand_filter = np.squeeze([np.random.rand(dim) < p])

    # child_weight_biases[rand_filter] += np.multiply(child_weight_biases[rand_filter], randMat[rand_filter])
    child_weight_biases[rand_filter] += randMat[rand_filter]

    return child_weight_biases

def mutation_rand_all_para(children_wb: np.array, p: float, scale=10, scale_by_weight:bool=True):
    mutationMat = np.random.rand(*children_wb.shape) * 2 * scale - scale
    if scale_by_weight:
        mutationMat = mutationMat * children_wb
    filt = np.random.rand(*children_wb.shape) < p

    return np.multiply(mutationMat, filt)


def ranking_selection(population: List[Individual], fitness_arr=None) -> Tuple[Individual, Individual]:
    return sorted(population, key=lambda individual: individual.fitness, reverse=True)
    sorted_population = sorted(population, key=lambda individual: individual.fitness, reverse=True)
    parent1, parent2 = sorted_population[:2]
    return parent1, parent2

def k_max_els(fitness_arr, k):
    return np.argpartition(fitness_arr, -k)[-k:]

def ranking_selectionMat(population: List[Individual], fitness_arr, k=3):
    dim = len(population)

    id = k_max_els(fitness_arr, k)

    pick = np.tile(id, int(np.ceil(2 * dim / k)))
    np.random.shuffle(pick)
    pick = np.reshape(pick[:2*dim], [2, dim])

    return pick

"""
Pick up random individual in a population.

Bugs:
    shifted values based on minimal. This avoids negative
    probabilities while keeping normal distribution
"""
def roulette_wheel_selection(population: List[Individual], fitness_arr):
    # Find the minimal fitness value
    min_fitness = min(fitness_arr)
    fitness_values = fitness_arr + abs(min_fitness) + 5

    fitness_values = np.abs(fitness_values)

    total_fitness = np.sum(fitness_values)
    selection_probabilities = np.divide(fitness_values, total_fitness)

    pick = np.random.choice(len(population), p=selection_probabilities)
    return population[pick]

def roulette_wheel_selection_pickMat(population: List[Individual], fitness_arr):
    # Find the minimal fitness value
    min_fitness = min(fitness_arr)
    fitness_values = fitness_arr + abs(min_fitness) + 5           # Add minimal fitness value to make all values greater than zero

    fitness_values = np.abs(fitness_values)

    total_fitness = np.sum(fitness_values)
    selection_probabilities = np.divide(fitness_values, total_fitness)

    # precalculate all parents that will be picked
    pick = np.random.choice(len(population), size=[2, len(population)], p=selection_probabilities)
    return pick

def selection_power_all(population: List[Individual], fitness_arr, pow=1.2):
    # Find the minimal fitness value
    min_fitness = min(fitness_arr)
    fitness_values = fitness_arr + abs(min_fitness) + 5           # Add minimal fitness value to make all values greater than zero

    # power
    fitness_values = np.power(fitness_values, pow)

    total_fitness = np.sum(fitness_values)
    selection_probabilities = np.divide(fitness_values, total_fitness)

    tmp = np.random.choice(population, size=[len(population)], p=selection_probabilities)

    return np.array([x.weights_biases for x in tmp])

def statistics(population: List[Individual], fitness=np.array([])):
    if fitness.size == 0:
        fitness = [individual.fitness for individual in population]
        
    return np.mean(fitness), np.min(fitness), np.max(fitness)