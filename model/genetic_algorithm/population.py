import copy
from datetime import datetime
from typing import Callable
from collections import namedtuple

import numpy as np
import torch

try:
    from sklearn.metrics.pairwise import euclidean_distances
    sklearn_available = True
except ImportError:
    sklearn_available = False

from genetic_algorithm.individual import *
from util.timing import timing


class Population:
    def __init__(self, Individual, FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):
        np.set_printoptions(precision=1)
        
        self.PARAMETERS = PARAMETERS
        self.FUNCTIONS = FUNCTIONS
        self.SETTINGS = SETTINGS
        
        self.pop_size = PARAMETERS.population_size
        self.max_generation = PARAMETERS.max_generation
        self.p_mutation = PARAMETERS.mutation_rate
        self.p_crossover = PARAMETERS.crossover_rate
        self.p_inversion = 0
        self.old_population = [copy.copy(Individual) for _ in range(self.pop_size)]
        self.new_population = []

        self.use_temp = SETTINGS.use_temp
        self.t = 0
        self.start_temp = PARAMETERS.start_temp
        self.decay_temp = PARAMETERS.decay_temp

        self.temp = 10

        self.past_mean = np.full(self.max_generation, -np.inf)

        self.keep_best = PARAMETERS.keep_best
        self.calc_fitness_rep = PARAMETERS.calc_fitness_rep
        self.only_better = SETTINGS.only_better
        self.selection_pow = PARAMETERS.selection_pow
        self.scale_by_weight = SETTINGS.scale_by_weight

        run_func = getattr(self, FUNCTIONS.run, None)
        if run_func:
            self.run_func = run_func
        else:
            raise AttributeError(f"Methods '{FUNCTIONS.run}' is not defined.")
        
        generation_func = getattr(self, FUNCTIONS.generation, None)
        if generation_func:
            self.generation_func = generation_func
        else:
            raise AttributeError(f"Methods '{FUNCTIONS.generation}' is not defined.")
        
        selection_func = globals().get(FUNCTIONS.selection)
        if selection_func:
            self.selection_func = selection_func
        else:
            raise AttributeError(f"Methods '{FUNCTIONS.selection}' is not defined.")
        
        crossover_func = globals().get(FUNCTIONS.crossover)
        if crossover_func:
            self.crossover_func = crossover_func
        else:
            raise AttributeError(f"Methods '{FUNCTIONS.crossover}' is not defined.")
        
        mutation_func = globals().get(FUNCTIONS.mutation)
        if mutation_func:
            self.mutation_func = mutation_func
        else:
            raise AttributeError(f"Methods '{FUNCTIONS.crossover}' is not defined.")
        

    def set_population(self, population: list):
        self.old_population = population

    @timing
    def run(self, env):
        # for i in range(self.pop_size):
        #     self.old_population[i].update_model()
        #     self.old_population[i].calculate_fitness(env)
        #     print(self.old_population[i].fitness)

        # [p.calculate_fitness(env) for p in self.old_population]

        best_model = sorted(self.old_population, key=lambda ind: ind.fitness, reverse=True)[0]
        for i in range(self.max_generation):
            [p.calculate_fitness(env) for p in self.old_population]

            old_fitness = np.array([p.fitness for p in self.old_population])

            self.new_population = [None for _ in range(self.pop_size)]  

            if self.use_temp:
                self.temp = self.start_temp * np.exp(-self.t * self.decay_temp)
                self.t += 1

            # self.generation(env)
            self.generation_func(env, old_fitness)

            if self.use_temp and self.SETTINGS.anti_stagnation:
                self.past_mean[i] = np.mean(old_fitness)
                if i > 11 and np.std(self.past_mean[i-10:i]) < 0.1:
                    self.t = int(self.t / 2)

            if self.SETTINGS.log and i % self.SETTINGS.log_frequency == 0:
                self.save_logs(i)

            if self.SETTINGS.verbose:
                self.show_stats(i)

            self.update_old_population()

            new_best_model = self.get_best_model_parameters()

            if self.SETTINGS.incremental_saving and new_best_model.fitness > best_model.fitness:
                print('Saving new best model with fitness: {}'.format(new_best_model.fitness))
                self.save_model_parameters(i)
                best_model = new_best_model

        
        self.save_model_parameters(self.max_generation)

    @timing
    def run2(self, env):
        [p.calculate_fitness(env, self.calc_fitness_rep) for p in self.old_population]
        fitness = np.array([p.fitness for p in self.old_population])

        best_model = sorted(self.old_population, key=lambda ind: ind.fitness, reverse=True)[0]
        for i in range(self.max_generation):
            self.new_population = None
            self.new_population = [None for _ in range(self.pop_size)]  

            if self.use_temp:
                self.temp = self.start_temp * np.exp(-self.t * self.decay_temp)
                self.t += 1

            self.generation_func(env, fitness, self.keep_best)
            self.old_population = None

            fitness = np.array([p.fitness for p in self.new_population])

            if self.use_temp and self.SETTINGS.anti_stagnation:
                self.past_mean[i] = np.mean(fitness)
                if i > 11 and np.std(self.past_mean[i-10:i]) < 0.1:
                    self.t = int(self.t / 2)

            if self.SETTINGS.log and i % self.SETTINGS.log_frequency == 0:
                self.save_logs(i, fitness)

            if self.SETTINGS.verbose:
                self.show_stats(i, fitness)

            self.update_old_population()

            new_best_model = self.get_best_model_parameters()

            if self.SETTINGS.incremental_saving and new_best_model.fitness > best_model.fitness:
                print('Saving new best model with fitness: {}'.format(new_best_model.fitness))
                self.save_model_parameters(i)
                best_model = new_best_model

        self.save_model_parameters(self.max_generation)



    def generation(self, env, fitness, start=0):
        # precalculate all parents that will be picked
        pick = self.selection_func(self.old_population, fitness)
        
        for i in range(start, len(self.old_population) - 1, 2):
            # Selection
            parent1 = self.old_population[pick[0, i]]
            parent2 = self.old_population[pick[1, i]]

            # parent1 = roulette_wheel_selection(self.old_population)
            # parent2 = roulette_wheel_selection(self.old_population)
            # parent1, parent2 = ranking_selection(self.old_population)

            # Crossover
            child1 = copy.deepcopy(parent1)
            child2 = copy.deepcopy(parent2)            
            child1.weights_biases, child2.weights_biases = self.crossover_func(parent1.weights_biases,
                                                        parent2.weights_biases,
                                                        self.p_crossover)

            child1.weights_biases = self.mutation_func(child1.weights_biases, self.p_mutation, scale=self.temp)
            child2.weights_biases = self.mutation_func(child2.weights_biases, self.p_mutation, scale=self.temp)

            # Update model weights and biases
            child1.update_model()
            child2.update_model()

            child1.calculate_fitness(env)
            child2.calculate_fitness(env)

            # If children fitness is greater thant parents update population
            if child1.fitness + child2.fitness > parent1.fitness + parent2.fitness:
                self.new_population[i] = child1
                self.new_population[i + 1] = child2
            else:
                self.new_population[i] = parent1
                self.new_population[i + 1] = parent2
            if child1.fitness > parent1.fitness:
                self.new_population[i] = child1
            if child2.fitness > parent2.fitness:
                self.new_population[i+1] = child2

    def generation_simp(self, env, fitness, start=2):
        parents = self.selection_func(self.old_population, fitness, self.selection_pow)

        if start > 0:
            id = k_max_els(fitness, start)
            for i in range(start):
                parents[i] = self.old_population[id[i]].weights_biases

        children_wb = self.crossover_func(parents, self.p_crossover, start)

        children_wb[start:] += self.mutation_func(children_wb, self.p_mutation, scale=self.temp, scale_by_weight=self.scale_by_weight)[start:]

        self.new_population = copy.deepcopy(self.old_population)

        for i, child in enumerate(self.new_population):
            child.weights_biases = children_wb[i]
            child.update_model()
            child.calculate_fitness(env, self.calc_fitness_rep)

            if self.only_better and (child.fitness < self.old_population[i].fitness):
                child = copy.deepcopy(self.old_population[i])
                child.update_model()
                child.calculate_fitness(env, self.calc_fitness_rep)

    """
    Random function
    """           
    def generation_random(self, env, fitness, start=0):
        # precalculate all parents that will be picked
        pick = roulette_wheel_selection_pickMat(self.old_population, fitness)

        for i in range(start, len(self.old_population) - 1, 2):
            # Selection
            parent1 = self.old_population[pick[0, i]]
            parent2 = self.old_population[pick[1, i]]

            # Crossover
            child1 = copy.deepcopy(parent1)
            child2 = copy.deepcopy(parent2)            
            child1.weights_biases, child2.weights_biases = self.crossover_func(parent1.weights_biases,
                                                        parent2.weights_biases,
                                                        self.p_crossover)

            child1.weights_biases = self.mutation_func(child1.weights_biases, self.p_mutation, scale=self.temp)
            child2.weights_biases = self.mutation_func(child2.weights_biases, self.p_mutation, scale=self.temp)

            # Update model weights and biases
            child1.update_model()
            child2.update_model()

            child1.calculate_fitness(env)
            child2.calculate_fitness(env)

            # If children fitness is greater than parents update population
            if child1.fitness + child2.fitness > parent1.fitness + parent2.fitness:
                self.new_population[i] = child1
                self.new_population[i + 1] = child2
            else:
                self.new_population[i] = parent1
                self.new_population[i + 1] = parent2

    """
    Partition function

    Feature:
        The function considers only the best half of the population
        and set directly the children into the new one.
        This let the program go faster because the children fitness is not
        recalculated and no condition to let the children in must be checked.
    """           
    def generation_partitioned(self, env, fitness, start=0):
        # precalculate all parents that will be picked
        assert(len(self.old_population) % 4 == 0)
        pick = self.selection_func(self.old_population, fitness)

        j = 0
        for i in range(start, len(self.old_population) - 1, 4):
            # Selection
            parent1 = pick[j]
            parent2 = pick[j + 1]
            j += 2

            # Crossover
            child1 = copy.deepcopy(parent1)
            child2 = copy.deepcopy(parent2)            
            child1.weights_biases, child2.weights_biases = self.crossover_func(parent1.weights_biases,
                                                        parent2.weights_biases,
                                                        self.p_crossover)

            child1.weights_biases = self.mutation_func(child1.weights_biases, self.p_mutation, scale=self.temp)
            child2.weights_biases = self.mutation_func(child2.weights_biases, self.p_mutation, scale=self.temp)

            # Update model weights and biases
            child1.update_model()
            child2.update_model()

            # Insert children and parents
            self.new_population[i] = parent1
            self.new_population[i + 1] = parent2
            self.new_population[i + 2] = child1
            self.new_population[i + 3] = child2

    def generation_best_two(self, env, fitness, start=0):
        # precalculate all parents that will be picked
        pick = self.selection_func(self.old_population, fitness)

        for i in range(start, len(self.old_population) - 1, 2):
            # Selection
            parent1 = pick[0]
            parent2 = pick[1]

            # Crossover
            child1 = copy.deepcopy(parent1)
            child2 = copy.deepcopy(parent2)            
            child1.weights_biases, child2.weights_biases = self.crossover_func(parent1.weights_biases,
                                                        parent2.weights_biases,
                                                        self.p_crossover)

            child1.weights_biases = self.mutation_func(child1.weights_biases, self.p_mutation, scale=self.temp)
            child2.weights_biases = self.mutation_func(child2.weights_biases, self.p_mutation, scale=self.temp)

            # Update model weights and biases
            child1.update_model()
            child2.update_model()

            child1.calculate_fitness(env)
            child2.calculate_fitness(env)

            # If children fitness is greater thant parents update population
            if child1.fitness + child2.fitness > parent1.fitness + parent2.fitness:
                self.new_population[i] = child1
                self.new_population[i + 1] = child2
            else:
                self.new_population[i] = parent1
                self.new_population[i + 1] = parent2
            if child1.fitness > parent1.fitness:
                self.new_population[i] = child1
            if child2.fitness > parent2.fitness:
                self.new_population[i+1] = child2

    def save_logs(self, n_gen, fitness=np.array([])):
        """
        CSV format -> n_generation, mean, min, max
        """
        mean, min, max = statistics(self.new_population, fitness)
        
        if sklearn_available and self.SETTINGS.diversity:
            distances = euclidean_distances([ind.weights_biases for ind in self.new_population])
            diversity = np.mean(distances)
        else: 
            diversity = float('nan')
        
        stats = f'| Gen: {n_gen:05d} | Mean: {mean:7.1f} | Min: {min:7.1f} | Max: {max:7.1f} | Temp: {self.temp:7.3f} | Div: {diversity:7.3f} |\n'
        with open(self.SETTINGS.output_file + ".out", 'a') as f:
            f.write(stats)

    def show_stats(self, n_gen, fitness=np.array([])):
        mean, min, max = statistics(self.new_population, fitness)
        
        if sklearn_available and self.SETTINGS.diversity:
            distances = euclidean_distances([ind.weights_biases for ind in self.new_population])
            diversity = np.mean(distances)
        else: 
            diversity = float('nan')
        
        date = self.now()

        if self.use_temp:
            mean_color = "\033[92m" if (n_gen > 0 and (mean > self.past_mean[n_gen-1])) else "\033[91m"
            reset_color = "\033[0m"

            stats = (
                f"{date} - generation {n_gen:05d} temp {self.temp:.3f} | \t"
                f"mean: {mean_color}{mean:.1f}{reset_color}\tmin: {min:.1f}\tmax: {max:.1f}\tdiv: {diversity:.1f}\n"
            )
        else:
            mean_color = "\033[92m" if mean > 0 else ""
            reset_color = "\033[0m"
            stats = f"{date} - generation {n_gen + 1} | \t mean: {mean_color}{mean:.1f}{reset_color}\tmin: {min:.1f}\tmax: {max:.1f}\tdiv: {diversity:.1f}\n"

        print(stats)

    def update_old_population(self):
        self.old_population = copy.deepcopy(self.new_population)

    def save_model_parameters(self, iterations):
        best_model = self.get_best_model_parameters()
        if self.SETTINGS.save_as_pytorch:
            torch.save(best_model.weights_biases, self.SETTINGS.output_file + "_model.npy")
        else:
            np.save(self.SETTINGS.output_file + "_model.npy", best_model.weights_biases)

    def get_best_model_parameters(self) -> np.array:
        """
        :return: Weights and biases of the best individual
        """
        return sorted(self.new_population, key=lambda ind: ind.fitness, reverse=True)[0]

    @staticmethod
    def now():
        return datetime.now().strftime('%m-%d-%Y_%H-%M')

    def visualize(self, env):
        for i in range(len(self.old_population) - 1):
            self.old_population[i].run_single(env)