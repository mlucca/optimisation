import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d 
import pandas as pd
import re
import sys


def plot_fitness_function(data, path):
    plt.figure(figsize=(8, 6))
    plt.grid(alpha=0.3)
    data_mean_smoothed = gaussian_filter1d(data["mean"], sigma=2)
    plt.plot(data["generation"], data["mean"], alpha=0.25, color='orange')
    plt.plot(data["generation"], data_mean_smoothed, label="Mean value", color='orange')
    plt.ylabel('Fitness function', fontsize=13)
    plt.xlabel('Generation', fontsize=13)
    plt.legend(fontsize=14)
    plt.tick_params(axis='both', which='major', labelsize=13)

    plt.savefig(path + "_fitness_plot.png", bbox_inches='tight', dpi=300)


def load_logs(path: str):
    data = []

    with open(path + ".out", 'r') as file:
        lines = file.readlines()

    # Skip header
    data_lines = lines[5:]

    # Process each data line
    for line in data_lines:
        match = re.match(r'\| Gen: (\d+) \| Mean:\s*(-?\d+\.\d+) \| Min:\s*(-?\d+\.\d+) \| Max:\s*(-?\d+\.\d+) \| Temp:\s*(-?\d+\.\d+)(?: \| Div:\s*(-?\d+\.\d+))?\s*\|', line)
        if match:
            generation, mean, min_val, max_val, temp, div = match.groups()
            if div is None:
                div = float('nan')  # Assign NaN if Div is not present
            data.append([int(generation), float(mean), float(min_val), float(max_val), float(temp), float(div)])

    df = pd.DataFrame(data, columns=['generation', 'mean', 'min', 'max', 'temp', 'div'])
    return df


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <path_to_logs>")
        sys.exit(1)

    path = sys.argv[1]
    data = load_logs(path)
    plot_fitness_function(data, path)
