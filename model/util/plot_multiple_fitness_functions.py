import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d 
import pandas as pd
import re
import sys
import os
import numpy as np
from collections import defaultdict


def parse_header(file_path):
    header_info = {}
    with open(file_path, 'r') as file:
        lines = file.readlines()
        in_functions = False
        in_parameters = False

        for line in lines:
            if 'Functions = [' in line:
                in_functions = True
                continue
            if 'Parameters = [' in line:
                in_parameters = True
                continue
            if ']' in line:
                in_functions = False
                in_parameters = False

            if in_functions or in_parameters:
                match = re.match(r'\s*(\w+)\s*=\s*[\'\"]?([\w\.]+)[\'\"]?\s*,?', line)
                if match:
                    key, value = match.groups()
                    header_info[key] = value
                    
            if 'OUTPUTS:' in line:
                break
            
    return header_info

def compare_headers(header_list):
    comparison_dict = defaultdict(set)
    for header in header_list:
        for key, value in header.items():
            comparison_dict[key].add(value)
    
    differing_parameters = {key: values for key, values in comparison_dict.items() if len(values) > 1}
    return differing_parameters

def plot_fitness_function(data_list, path, differing_parameters):
    plt.figure(figsize=(10, 8))
    plt.grid(alpha=0.3)

    colors = plt.colormaps.get_cmap('tab10')  # Get a colormap with enough unique colors
    sorted_data_list = sorted(data_list, key=lambda x: x[2])    

    for idx, (data, header, name) in enumerate(sorted_data_list):
        label = name + ":  " + ", ".join(f"{key}={header[key]}" for key in differing_parameters.keys())
        color = colors(idx)
        data_mean_smoothed = gaussian_filter1d(data["mean"], sigma=2)
        plt.plot(data["generation"], data["mean"], alpha=0.25, color=color)
        plt.plot(data["generation"], data_mean_smoothed, label=label, color=color)
    
    plt.ylabel('Fitness function', fontsize=13)
    plt.xlabel('Generation', fontsize=13)
    plt.legend(fontsize=10, loc='best')
    plt.tick_params(axis='both', which='major', labelsize=13)
    plt.title("/".join(path.split("/")[-2:]), fontsize=15)

    plt.savefig(os.path.join(path, "combined_fitness_plot.png"), bbox_inches='tight', dpi=300)
    #plt.show()

def plot_best(data_list, path, differing_parameters):
    plt.figure(figsize=(10, 8))
    plt.grid(alpha=0.3)

    colors = plt.colormaps.get_cmap('tab10')  # Get a colormap with enough unique colors
    sorted_data_list = sorted(data_list, key=lambda x: x[2])

    best = 0
    sum_best = float('-inf')
    max_val_best = float('-inf')
    for idx, (data, header, name)in enumerate(sorted_data_list):
        sum = 0.
        i = 0
        max_val = max(data["mean"])
        if(max_val > max_val_best):
            max_val_best = max_val
            best = idx
        # for _ in range(len(data)):
            # sum += float(data["mean"][i])
            # if(max_val > max_val_best):
            #     max_val_best = max_val
            #     best = idx
            # i += 1

    for idx, (data, header, name) in enumerate(sorted_data_list):
        label = name + ":  " + ", ".join(f"{key}={header[key]}" for key in differing_parameters.keys())
        if(best != idx):
            data_mean_smoothed = gaussian_filter1d(data["mean"], sigma=2)
            plt.plot(data["generation"], data_mean_smoothed, alpha=0.15, color='grey') # Removed label
    for idx, (data, header, name) in enumerate(sorted_data_list): # Print best graph above everything else
        label = name + ":  " + ", ".join(f"{key}={header[key]}" for key in differing_parameters.keys())
        if(best == idx):
            color = colors(idx)
            data_mean_smoothed = gaussian_filter1d(data["mean"], sigma=2)
            plt.plot(data["generation"], data_mean_smoothed, label=label, color="red")
    
    plt.ylabel('Fitness function', fontsize=13)
    plt.xlabel('Generation', fontsize=13)
    plt.legend(fontsize=10, loc='best')
    plt.tick_params(axis='both', which='major', labelsize=13)
    plt.title("/".join(path.split("/")[-2:]), fontsize=15)

    plt.savefig(os.path.join(path, "best_plot.png"), bbox_inches='tight', dpi=300)


def load_logs(file_path):
    data = []

    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Skip header
    data_lines = lines[5:]

    # Process each data line
    for line in data_lines:
        match = re.match(r'\| Gen: (\d+) \| Mean:\s*(-?\d+\.\d+) \| Min:\s*(-?\d+\.\d+) \| Max:\s*(-?\d+\.\d+) \| Temp:\s*(-?\d+\.\d+)(?: \| Div:\s*(-?\d+\.\d+))?\s*\|', line)
        if match:
            generation, mean, min_val, max_val, temp, div = match.groups()
            if div is None:
                div = float('nan')  # Assign NaN if Div is not present
            data.append([int(generation), float(mean), float(min_val), float(max_val), float(temp), float(div)])
    
    df = pd.DataFrame(data, columns=['generation', 'mean', 'min', 'max', 'temp', 'div'])
    return df


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <path_to_folder>")
        sys.exit(1)

    folder_path = sys.argv[1]
    data_list = []
    header_list = []

    for file_name in os.listdir(folder_path):
        if file_name.endswith(".out"):
            file_path = os.path.join(folder_path, file_name)
            header = parse_header(file_path)
            data = load_logs(file_path)
            name = file_name.split(".")[0]
            header_list.append(header)
            data_list.append((data, header, name))
    
    if data_list:
        differing_parameters = compare_headers(header_list)
        plot_fitness_function(data_list, folder_path, differing_parameters)
        plot_best(data_list, folder_path, differing_parameters)
    else:
        print("No .out files found in the specified folder.")
