import os
import sys
import sys
import re

import gymnasium as gym

from collections import namedtuple

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(current_dir, '..')
sys.path.append(parent_dir)

from neural_network.base_nn import NeuralNetwork
from neural_network.mlp import DeepMLPTorch
from typing import Tuple

import games

def lunar_lander_vis(Path, n):
    env = gym.make("LunarLander-v2", render_mode="human")

    inv = games.MLPLunarLander(Path)

    for i in range(n):
        inv.run_single(env, render=True)
        
    env.close()

def car_racing_vis(Path, n):
    env = gym.make("CarRacing-v2", continuous=True, render_mode="human")

    inv = games.MLPCarRacing(Path)

    for i in range(n):
        inv.run_single(env)
        
    env.close()

def bipedal_walker_vis(Path, n):
    env = gym.make("BipedalWalker-v3", render_mode="human")

    inv = games.MLPBipedalWalker(Path)

    for i in range(n):
        inv.run_single(env)
        
    env.close()

def acrobot_vis(Path, n):
    env = gym.make("Acrobot-v1", render_mode="human")

    inv = games.MLPAcrobot(Path)

    for i in range(n):
        inv.run_single(env)
        
    env.close()
    
def cart_pole_vis(Path, n):
    env = gym.make("CartPole-v1", render_mode="human")

    inv = games.MLPCartPole(Path)

    for i in range(n):
        inv.run_single(env)
        
    env.close()

def extract_game_name(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
    
    game_match = re.search(r"game\s*=\s*'([^']+)'", content)
    
    if game_match:
        game_name = game_match.group(1)
        return game_name
    else:
        return None

if len(sys.argv) < 2:
    print("Please provide the path to the model file")
    sys.exit(1)
else:
    Path = sys.argv[1]
    
if len(sys.argv) == 3:
    n = int(sys.argv[2])
else:
    n = 100

game = extract_game_name(Path + ".out")

if game == "lunar_lander":
    lunar_lander_vis(Path + "_model.npy", n)
elif game == "car_racing":
    car_racing_vis(Path + "_model.npy", n)
elif game == "bipedal_walker":
    bipedal_walker_vis(Path + "_model.npy", n)
elif game == "acrobot":
    acrobot_vis(Path + "_model.npy", n)
elif game == "cart_pole":
    cart_pole_vis(Path + "_model.npy", n)