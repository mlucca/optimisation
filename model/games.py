import copy
import sys
import os
import torch
from collections import namedtuple

import gymnasium as gym
import numpy as np

from typing import Tuple
from genetic_algorithm.individual import *
from genetic_algorithm.population import Population
from neural_network.base_nn import NeuralNetwork
from neural_network.mlp import DeepMLPTorch

class MLPLunarLander(Individual):
    def __init__(self, old_weights_path=""):
        self.input_size = 8
        self.hidden_size = [16,4]
        self.output_size = 4  # Changed output size to 4
        self.old_weights_path = old_weights_path
        super().__init__()

    def get_model(self) -> NeuralNetwork:
        return DeepMLPTorch(self.old_weights_path, self.input_size, self.output_size, *(self.hidden_size))

    def run_single(self, env, n_episodes=300, render=False) -> Tuple[float, np.array]:
        obs = env.reset()
        obs = obs[0]
        fitness = 0
        for _ in range(n_episodes):
            if render:
                env.render()
            
            # Unpacked obs and switched to numpy array
            obs = np.array(obs[:self.input_size])
            obs = torch.from_numpy(obs).float()
            action = self.neural_network.forward(obs)
            action = action.detach().numpy()
            
            # Choose action with highest output neuron
            action = np.argmax(action)

            obs, reward, done, _, _ = env.step(action)
            fitness += reward
            if done:
                break
        return fitness, self.neural_network.get_weights_biases()

class MLPCarRacing(Individual):
    def __init__(self, old_weights_path=""):
        self.input_size = 3
        self.hidden_size = [9,5]
        self.output_size = 3
        self.old_weights_path = old_weights_path
        super().__init__()

    def get_model(self) -> NeuralNetwork:
        return DeepMLPTorch(self.old_weights_path, self.input_size, self.output_size, *(self.hidden_size))

    def run_single(self, env, n_episodes=100, render=False) -> Tuple[float, np.array]:
        obs = env.reset()
        obs = obs[0]
        fitness = 0
        for episode in range(n_episodes):
            if render:
                env.render()
            
            # Unpacked obs and switched to numpy array
            obs = np.array(obs[:self.input_size])
            obs = torch.from_numpy(obs).float()
            action = self.neural_network.forward(obs)
            action = action.detach().numpy()

            # Make action discrete for step
            # discrete_action = round((action[0][0][0]+1) * 4/2)
            # action = min(discrete_action, 3)

            # print("action",action)
            # if(action < 0):
            #     action = 0
            
            obs, reward, done, _, _ = env.step(action[0][0])
            fitness += reward
            if done:
                break
        return fitness, self.neural_network.get_weights_biases()
    
class MLPBipedalWalker(Individual):
    def __init__(self, old_weights_path=""):
        self.input_size = 24
        self.hidden_size = [32,16,8] 
        self.output_size = 4
        self.old_weights_path = old_weights_path
        super().__init__()

    def get_model(self) -> NeuralNetwork:
        return DeepMLPTorch(self.old_weights_path, self.input_size, self.output_size, *(self.hidden_size))

    def run_single(self, env, n_episodes=300, render=False) -> Tuple[float, np.array]:
        obs = env.reset()
        obs = obs[0]
        fitness = 0
        for episode in range(n_episodes):
            if render:
                env.render()
            
            # Unpacked obs and switched to numpy array
            obs = np.array(obs[:self.input_size])
            obs = torch.from_numpy(obs).float()
            action = self.neural_network.forward(obs)
            action = action.detach().numpy()
                        
            obs, reward, done, _, _ = env.step(action)
            fitness += reward
            if done:
                break
        return fitness, self.neural_network.get_weights_biases()
    
class MLPAcrobot(Individual):
    def __init__(self, old_weights_path=""):
        self.input_size = 4
        self.hidden_size = [12,8,4] 
        self.output_size = 3
        self.old_weights_path = old_weights_path
        super().__init__()

    def get_model(self) -> NeuralNetwork:
        return DeepMLPTorch(self.old_weights_path, self.input_size, self.output_size, *(self.hidden_size))

    def run_single(self, env, n_episodes=300, render=False) -> Tuple[float, np.array]:
        obs = env.reset()
        obs = obs[0]
        fitness = 0
        for episode in range(n_episodes):
            if render:
                env.render()
            
            # Unpacked obs and switched to numpy array
            obs = np.array(obs[:self.input_size])
            obs = torch.from_numpy(obs).float()
            action = self.neural_network.forward(obs)
            action = action.detach().numpy()
            
            action = np.argmax(action)
                        
            obs, reward, done, _, _ = env.step(action)
            fitness += reward
            if done:
                break
        return fitness, self.neural_network.get_weights_biases()

class MLPCartPole(Individual):
    def __init__(self, old_weights_path=""):
        self.input_size = 4
        self.hidden_size = [16,4] 
        self.output_size = 1
        self.old_weights_path = old_weights_path
        super().__init__()

    def get_model(self) -> NeuralNetwork:
        return DeepMLPTorch(self.old_weights_path, self.input_size, self.output_size, *(self.hidden_size))

    def run_single(self, env, n_episodes=600, render=False) -> Tuple[float, np.array]:
        obs = env.reset()
        obs = obs[0]
        fitness = 0
        for episode in range(n_episodes):
            if render:
                env.render()
            
            # Unpacked obs and switched to numpy array
            obs = np.array(obs[:self.input_size])
            obs = torch.from_numpy(obs).float()
            action = self.neural_network.forward(obs)
            action = action.detach().numpy()
            move = 0
            if action >= 0:
                move = 1
                        
            obs, reward, done, _, _ = env.step(move)
            fitness += reward
            if done:
                break
        return fitness, self.neural_network.get_weights_biases()

def lunar_lander_main(FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):

   env = gym.make("LunarLander-v2", enable_wind = False, wind_power = 19.0)
   
   p = Population(MLPLunarLander(SETTINGS.load_weights_path), FUNCTIONS, PARAMETERS, SETTINGS)
  
   p.run_func(env)

   env.close()

def car_racing_main(FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):
    
   env = gym.make("CarRacing-v2",
                   continuous=True)

   p = Population(MLPCarRacing(SETTINGS.load_weights_path), FUNCTIONS, PARAMETERS, SETTINGS)
   p.run_func(env)

   env.close()

def bipedal_walker_main(FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):
    
   env = gym.make("BipedalWalker-v3")

   p = Population(MLPBipedalWalker(), FUNCTIONS, PARAMETERS, SETTINGS)
   p.run_func(env)

   env.close()

def acrobot_main(FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):
   
   env = gym.make('Acrobot-v1')

   p = Population(MLPAcrobot(), FUNCTIONS, PARAMETERS, SETTINGS)
   p.run_func(env)
   
def cart_pole_main(FUNCTIONS: namedtuple, PARAMETERS: namedtuple, SETTINGS: namedtuple):
   
   env = gym.make('CartPole-v1')
   
   p = Population(MLPCartPole(), FUNCTIONS, PARAMETERS, SETTINGS)
   p.run_func(env)

   env.close()